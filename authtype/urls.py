from django.contrib import admin
from django.urls import path, include
from core.views import register, email_login, create_otp, otp_login, FacebookView, HelloView, \
    GoogleSocialAuthView, TwitterSocialAuthView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HelloView.as_view()),
    path('register/', register),
    path('emaillogin/', email_login),
    path('createotp/', create_otp),
    path('otplogin/', otp_login),
    path('fb/', FacebookView.as_view()),
    path('google/', GoogleSocialAuthView.as_view()),
    path('twitter/', TwitterSocialAuthView.as_view())

]

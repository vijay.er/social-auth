from core.models import UserOTP


def otp_post_authentication(user, otp):
    otp = UserOTP.objects.get(user=user, otp=otp)
    otp.used = True
    otp.save()
    return True

from django.contrib.auth.models import AbstractUser
from django.db import models
from rest_framework_simplejwt.tokens import RefreshToken

AUTH_PROVIDERS = {'facebook': 'facebook', 'google': 'google',
                  'twitter': 'twitter', 'email': 'email and mobile number'}


class User(AbstractUser):
    mobile_number = models.CharField(max_length=200)
    auth_provider = models.CharField(
        max_length=255, blank=False,
        null=False, default=AUTH_PROVIDERS.get('email'))
    REQUIRED_FIELDS = ['mobile_number', 'email']

    def __str__(self):
        return self.username

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }


class UserOTP(models.Model):
    otp = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    used = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

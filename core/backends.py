from django.contrib.auth.models import User
from django.contrib.auth.backends import BaseBackend
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from .models import User, UserOTP
from .utils import otp_post_authentication


class CustomBackend(BaseBackend):
    def authenticate(self, request, email=None, password=None, otp=None, mobile_number=None, **kwargs):
        if email is not None and password is not None:
            if not User.objects.filter(email=email):
                raise NotFound({'error': 'user not found'})
            user = User.objects.filter(email=email)[0]
            if user.check_password(password):
                return user

        if mobile_number is not None and otp is not None:
            if not User.objects.filter(mobile_number=mobile_number):
                raise NotFound({'error': 'user not found'})
            user = User.objects.get(mobile_number=mobile_number)
            user_otp = UserOTP.objects.filter(user=user, used=False).last()
            print(user, user_otp, "otp")
            if user_otp is not None:
                if user_otp.otp == otp:
                    otp_post_authentication(user, otp)
                    return user


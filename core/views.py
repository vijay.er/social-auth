from random import randrange
from django.contrib.auth import authenticate, get_user_model
from rest_framework.decorators import api_view
from rest_framework.exceptions import NotFound
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings
from core.models import UserOTP
from core.serializers import UserRegisterSerializer, FacebookSocialAuthSerializer, TwitterAuthSerializer, \
    GoogleSocialAuthSerializer
from rest_framework import status, views
from rest_framework.response import Response
from django.db import transaction


User = get_user_model()


@api_view(['POST'])
def register(request, *args, **kwargs):
    try:
        with transaction.atomic():
            serializer = UserRegisterSerializer(data=request.POST)
            data = {}
            if serializer.is_valid():
                serializer.save()
                user = authenticate(email=request.data['email'], password=request.data['password'])
                print(user, "user")
                if user:
                    data['response'] = "successfully registered new user"
                    data['username'] = user.username
                    data['email'] = user.email
                    data['mobile_number'] = user.mobile_number
                    data['token'] = user.tokens()

            else:
                data = serializer.errors

            return Response(data)
    except Exception as e:
        print(e)
        return Response('Something went wrong', status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def email_login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        # username = User.objects.get(email=email)
        # print(username)
        user = authenticate(email=email, password=password)
        print(user)
        if user:
            data = {'response': "successfully you are logged in",
                    'username': user.username,
                    'email': user.email,
                    'mobile_number': user.mobile_number,
                    'token': user.tokens()
                    }
            return Response(data)
        else:
            data = {'response': "invalid user details"}
            return Response(data)


@api_view(['POST'])
def create_otp(request):
    if request.method == 'POST':
        mobile_number = request.POST['mobile_number']
        if not User.objects.filter(mobile_number=mobile_number):
            raise NotFound({'error': 'user not found'})
        user = User.objects.get(mobile_number=mobile_number)

        if user:
            random_number = randrange(99999, 999999)
            otp = UserOTP.objects.create(user=user, otp=random_number)
            return Response({'otp': otp.otp})
        else:
            return Response({'error': 'invalid user'})


@api_view(['POST'])
def otp_login(request):
    if request.method == 'POST':
        mobile_number = request.POST['mobile_number']
        otp = request.POST['otp']

        # user = User.objects.get(mobile_number=mobile_number)
        # print(user)
        user = authenticate(mobile_number=mobile_number, otp=otp)
        if user:
            data = {'response': "successfully you are logged in",
                    'username': user.username,
                    'email': user.email,
                    'mobile_number': user.mobile_number,
                    'token': user.tokens()
                    }
            return Response(data)
        else:
            data = {'response': "invalid user details"}
            return Response(data)


class FacebookView(views.APIView):
    """View to authenticate users through Facebook."""
    serializer_class = FacebookSocialAuthSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = (serializer.validated_data['auth_token'])
        return Response(data, status=status.HTTP_200_OK)


class HelloView(APIView):
    # permission_classes = (IsAuthenticated,)

    def get(self, request):
        print(request.user, "user")
        if request.user.is_authenticated:
            content = {'message': 'Hello ' + str(request.user)}
            return Response(content)
        else:
            content = {'message': 'Hello user! you are not logged in'}
            return Response(content)


class GoogleSocialAuthView(GenericAPIView):

    serializer_class = GoogleSocialAuthSerializer

    def post(self, request):
        """
        POST with "auth_token"
        Send an idtoken as from google to get user information
        """

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = (serializer.validated_data['auth_token'])
        return Response(data, status=status.HTTP_200_OK)


class TwitterSocialAuthView(GenericAPIView):
    serializer_class = TwitterAuthSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)







    # permission_classes = (permissions.AllowAny,)
    #
    # def post(self, request):
    #     auth_token = request.data['access_token']
    #     backend = request.data['backend']
    #     if auth_token and backend:
    #         try:
    #             # Try to authenticate the user using python-social-auth
    #             user = auth_by_token(request, backend)
    #         except:
    #             return Response({
    #                 'status': 'Bad request',
    #                 'message': 'Could not authenticate with the provided token.'
    #             }, status=status.HTTP_400_BAD_REQUEST)
    #         if user:
    #             if not user.is_active:
    #                 return Response({
    #                     'status': 'Unauthorized',
    #                     'message': 'The user account is disabled.'
    #                 }, status=status.HTTP_401_UNAUTHORIZED)
    #
    #             # This is the part that differs from the normal python-social-auth implementation.
    #             # Return the JWT instead.
    #
    #             # Get the JWT payload for the user.
    #             payload = jwt_payload_handler(user)
    #
    #             # Include original issued at time for a brand new token,
    #             # to allow token refresh
    #             if api_settings.JWT_ALLOW_REFRESH:
    #                 payload['orig_iat'] = timegm(
    #                     datetime.utcnow().utctimetuple()
    #                 )
    #
    #             # Create the response object with the JWT payload.
    #             response_data = {
    #                 'token': jwt_encode_handler(payload)
    #             }
    #
    #             return Response(response_data)
    #     else:
    #         return Response({
    #             'status': 'Bad request',
    #             'message': 'Authentication could not be performed with received data.'
    #         }, status=status.HTTP_400_BAD_REQUEST)
